#!/bin/bash
# Plot baz/inc plot at given time for given beam file
#if [ $# != 2 ]; then
#   echo "USAGE: ./polar_plot BEAMFILE TIME"
#   exit
#fi
#if [ ! -d slides ]; then mkdir slides; fi

file=$1
scale=Pa4.5i
region=0/48/3481/6371

#awk '{print $1,$2,sqrt(($3-672)^2)}' $file | gmt gmtinfo 

gmt makecpt -N -M -Cseis -T730/1200/50  -Z > cmap.cpt

gmt surface $file -R$region -Gtmp.grd -I1/50

gmt grdimage tmp.grd -R$region -J$scale -Ccmap.cpt -nl -K > out.ps

evince out.ps


